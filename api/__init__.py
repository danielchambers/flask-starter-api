from flask import Flask
from .v1.endpoints import api

app = Flask(__name__, instance_relative_config=True)

# Load the default configuration
app.config.from_object('configuration.default')

# Load the configuration from the instance folder
app.config.from_pyfile('configuration.py')

# Load the file specified by the APP_CONFIG_FILE environment variable
# Variables defined here will override those in the default configuration
app.config.from_envvar('APP_CONFIG_FILE')

app.register_blueprint(api, url_prefix='/v1')