import os.path

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DEBUG = True
TESTING = True
SERVER_NAME = '127.0.0.1:5000'